module study_consul

go 1.14

require (
	github.com/hashicorp/consul v1.7.2
	github.com/hashicorp/consul/api v1.4.0
)
