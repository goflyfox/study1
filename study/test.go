package main

import "fmt"

func main() {
	fmt.Println("hello world")
	var a = "123"
	fmt.Print(a)

	f := "aaa"
	fmt.Println(f, a)

	const a2 = 33
	fmt.Print(a2)

	for i := 0; i <= 10; i++ {
		fmt.Println(i)
	}

	for {
		fmt.Println("111")
		break
	}

	n := 1
	for n<3  {
		n++
		continue
		fmt.Println(1)
	}

	if n>1 {
		fmt.Println("n>1")
	} else if n < 0 {
		fmt.Println("n<0")
	} else {
		fmt.Println("else")
	}
}
