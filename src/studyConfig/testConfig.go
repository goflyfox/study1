package main

import (
	"fmt"
	"studyConfig/config"
)

func main() {
	config2 := new(config.IniConfig)
	configer,_ := config2.Parse("conf/app.conf")

	mysqluser := configer.String("mysqluser")
	mysqlpass := configer.String("mysqlpass")
	mysqlip := configer.String("mysqlip")
	mysqldb := configer.String("mysqldb")
	dataSource := mysqluser + ":" + mysqlpass + "@tcp(" + mysqlip + ":3306)/" + mysqldb + "?charset=utf8"
	fmt.Println(dataSource)
}
