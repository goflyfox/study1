package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("请输入文件名称：")
	for scanner.Scan() {
		ucl := scanner.Text()
		fmt.Println(ucl)

		path, name := getCurrentDirectory()
		getFilelist(path, name)

		break
	}

	// 按任意键退出
	fmt.Println("按任意键退出")
	scanner.Scan()
	os.Exit(3)

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	}
}

func getFilelist(path string,name string) {
	err := filepath.Walk(path, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		if f.Name() != name {
			fmt.Println(path)
		}
		return nil
	})
	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}
}

func getCurrentDirectory() (string, string) {
	fpath := os.Args[0];
	dir, err := filepath.Abs(filepath.Dir(fpath))
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(dir, "\\", "/", -1), filepath.Base(fpath)
}
