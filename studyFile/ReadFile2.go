package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"time"
)

func main() {
	start := time.Now().Unix();
	fi, err := os.Open("D:\\all.csv")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}
	defer fi.Close()

	br := bufio.NewReader(fi)
	for {
		a, _, c := br.ReadLine()
		if c == io.EOF {
			break
		}
		fmt.Println(a)
	}

	fmt.Printf("time:%d" ,(time.Now().Unix() - start))
}