下载go依赖
```bash
go get -u google.golang.org/grpc
go get -u github.com/golang/protobuf
go get -u github.com/golang/protobuf/protoc-gen-go
```

首先到 https://github.com/protocolbuffers/protobuf/releases 下载 相应的包，例如笔者下载的是 protoc-3.15.6-win64.zip。

解压后，复制里面的 bin\protoc.exe 文件，复制到 GOPATH\bin 命令，跟 protoc-gen-go.exe 放一起。

参考： https://www.cnblogs.com/whuanle/p/14588031.html