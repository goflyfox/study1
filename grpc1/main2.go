package main

import (
	"bufio"
	"context"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"grpc1/api"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:8028", grpc.WithInsecure())
	if err != nil {
		log.Fatal("连接 gPRC 服务失败,", err)
	}

	defer conn.Close()

	// 创建 gRPC 客户端
	grpcClient := api.NewTesterClient(conn)

	// 创建请求参数
	request := api.Request{
		JsonStr: `{"Code":666}`,
	}

	reader := bufio.NewReader(os.Stdin)

	out, err := proto.Marshal(&request)
	if err != nil {
		log.Fatalln("Failed to encode address book:", err)
	}
	if err := ioutil.WriteFile("D:/log.txt", out, 0644); err != nil {
		log.Fatalln("Failed to write address book:", err)
	}

	for {
		// 发送请求，调用 MyTest 接口
		response, err := grpcClient.MyTest(context.Background(), &request)
		if err != nil {
			log.Fatal("发送请求失败，原因是:", err)
		}
		log.Println(response)

		reader.ReadLine()
	}
}

type TesterServer interface {
	MyTest(context.Context, *api.Request) (*api.Response, error)
}

type TesterClient interface {
	MyTest(ctx context.Context, in *api.Request, opts ...grpc.CallOption) (*api.Response, error)
}
