package main

import (
	"io/ioutil"
	"fmt"
	"net/http"
)

func main() {
	httpGet()
}

func httpGet() {
	resp, err := http.Get("http://mtg.jflyfox.com/front/about")
	if err != nil {
		// handle error
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
	}

	fmt.Println(string(body))
}
