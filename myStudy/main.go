package main

import (
	"fmt"
	"encoding/xml"
	"os"
	"io/ioutil"
)

type Config struct {
	XMLName   xml.Name `xml:"config"`
	Version   int      `xml:"version,attr"`
	Name      string   `xml:"name"`
	Path	  string   `xml:"path"`
}

func main() {
	file, err := os.Open("config.xml")
	config := Config{}

	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	err = xml.Unmarshal(data, &config)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	fmt.Println(config)
	fmt.Println(config.Name)
	fmt.Println(config.Path)
}

