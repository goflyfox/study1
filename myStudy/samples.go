package main

import (
	"fmt"
)

const s string = "constant"
const s1 = "constant1"

func init() {
	fmt.Println(11)
}
func main() {
	fmt.Println(222)

	// value
	show := "show"
	fmt.Println(show)

	fmt.Println(s)
	fmt.Println(s1)

	// array
	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("dcl:", b)

	// Slices
	slice := make([]string, 3)
	fmt.Println("emp:", slice)

	sli2 := []string{"1", "2", "3"}
	fmt.Println("sli2:", sli2)
	sli2 = append(sli2, sli2[1:]...)
	fmt.Println("sli2:", sli2)
	sli2 = append(sli2, "d")
	fmt.Println("sli2:", sli2)

	// map
	n := map[string]int{"foo": 1, "bar": 2}
	fmt.Println("map:", n)

	m := make(map[string]int)
	m["k1"] = 7
	m["k2"] = 13
	fmt.Println("map:", m)

	// range
	nums := []int{2, 3, 4}
	for i, num := range nums {
		fmt.Println("range:", i, num)
	}

	// pointer
	i := 1
	fmt.Println("initial:", i)
	fmt.Println("pointer:", &i)

	// struct
	fmt.Println(person{"Bob", 20})
	fmt.Println(person{name: "Alice", age: 30}.name)
	fmt.Println(&person{name: "Ann", age: 40}) //  An & prefix yields a pointer to the struct.

	// Channels
	messages := make(chan string, 2)
	go func() { messages <- "ping" }()
	go func() { messages <- "ping2" }()
	msg := <-messages
	fmt.Println(msg)
	msg = <-messages
	fmt.Println(msg)
}

type person struct {
	name string
	age  int
}
