package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	chan1 := make(chan string)
	wait := sync.WaitGroup{}
	wait.Add(2)
	go func() {
		chan1 <- "OK1"
		time.Sleep(time.Second * 2)
		wait.Done()
	}()

	go func() {
		fmt.Println(<-chan1)
		wait.Done()
	}()

	fmt.Println(time.Now().Second())
	wait.Wait()
	fmt.Println(time.Now().Second())
	fmt.Println("finish")
}
