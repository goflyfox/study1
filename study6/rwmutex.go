package main

import (
	"errors"
	"fmt"
	"sync"
)

func main() {
	bank := RWBank{balance: map[string]float64{
		"a": 10000,
		"b": 20000,
	}}
	wait1 := sync.WaitGroup{}
	wait1.Add(40000)
	for i := 0; i < 10000; i++ {
		go func() {
			bank.In("a", 2)
			wait1.Done()
		}()
	}
	for i := 0; i < 10000; i++ {
		go func() {
			err := bank.Out("a", 1)
			if err != nil {
				panic(err)
			}
			wait1.Done()
		}()
	}

	for i := 0; i < 10000; i++ {
		go func() {
			err := bank.Out("b", 2)
			if err != nil {
				panic(err)
			}
			wait1.Done()
		}()
	}

	for i := 0; i < 100; i++ {
		go func() {
			fmt.Println(bank.Query("b"))
		}()
	}

	for i := 0; i < 10000; i++ {
		go func() {
			bank.In("b", 2)
			wait1.Done()
		}()
	}
	wait1.Wait()
	fmt.Println(bank.balance["a"], bank.balance["b"])

}

type RWBank struct {
	sync.RWMutex
	balance map[string]float64
}

func (b *RWBank) In(account string, value float64) {
	b.Lock()
	defer b.Unlock()

	if _, ok := b.balance[account]; !ok {
		b.balance[account] = 0.0
	}

	b.balance[account] += value
}

func (b *RWBank) Out(account string, value float64) error {
	b.Lock()
	defer b.Unlock()

	v, ok := b.balance[account]
	if !ok || v < value {
		return errors.New("account not enough balance")
	}

	b.balance[account] -= value
	return nil
}

func (b *RWBank) Query(account string) float64 {
	b.RLock()
	defer b.RUnlock()

	if _, ok := b.balance[account]; !ok {
		return 0.0
	}

	return b.balance[account]
}
